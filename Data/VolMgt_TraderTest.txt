[Volatility-Tracking]
WindowPosition=left 305 top 197 right 1704 bottom 862 Minimized 0
SplitPos=x 300 y 0
[Global]
IVBidCall=1
IVAskCall=1
VolCall=1
IVBidPut=1
IVAskPut=1
IVAverage=1
VolPut=1
RefCall=1
RefPut=1
PointSize=3
AutoScaleExpirations=1
AutoScaleVolat=0
Legend=1
CopyCommand=Copy from IV Average
AutoUpdateIV=0
AverageVol=1
[BTC]
LowLimit=0.000000
HighLimit=200.000000
Increment=0.100000
ShowAllExpirations=0
Expirations=
VisibleFamMembers=
[WorkState_v1_1]
ptn_Child1=DockState
ptn_Child2=ToolBarMgr
[WorkState_v1_1.DockState]
Bars=15
ScreenCX=3750
ScreenCY=950
ptn_Child1=Bar-0
ptn_Child2=Bar-1
ptn_Child3=Bar-2
ptn_Child4=Bar-3
ptn_Child5=Bar-4
ptn_Child6=Bar-5
ptn_Child7=Bar-6
ptn_Child8=Bar-7
ptn_Child9=Bar-8
ptn_Child10=Bar-9
ptn_Child11=Bar-10
ptn_Child12=Bar-11
ptn_Child13=Bar-12
ptn_Child14=Bar-13
ptn_Child15=Bar-14
[WorkState_v1_1.DockState.Bar-0]
BarID=59419
Bars=11
Bar#0=0
Bar#1=32768
Bar#2=0
Bar#3=59392
Bar#4=59397
Bar#5=59398
Bar#6=59399
Bar#7=59401
Bar#8=59400
Bar#9=59402
Bar#10=0
Style=0
ExStyle=0
PrevFloating=False
MDIChild=False
AutoHide=False
AutoHidePinned=False
LastAlignedDocking=0
PctWidth=0
MRUFloatCX=0
MRUFloatCY=0
MRUHorzDockCX=0
MRUHorzDockCY=0
MRUVertDockCX=0
MRUVertDockCY=0
MRUDockingState=0
DockingStyle=0
TypeID=0
ClassName=
WindowName=
ResourceID=0
[WorkState_v1_1.DockState.Bar-1]
BarID=32768
Visible=False
XPos=0
YPos=0
Docking=True
MRUDockID=0
MRUDockLeftPos=12
MRUDockTopPos=32
MRUDockRightPos=12
MRUDockBottomPos=32
MRUFloatStyle=4
MRUFloatXPos=-4
MRUFloatYPos=0
Style=12110
ExStyle=0
PrevFloating=False
MDIChild=False
AutoHide=False
AutoHidePinned=False
LastAlignedDocking=0
PctWidth=1000000
MRUFloatCX=300
MRUFloatCY=180
MRUHorzDockCX=300
MRUHorzDockCY=180
MRUVertDockCX=150
MRUVertDockCY=180
MRUDockingState=0
DockingStyle=8192
TypeID=0
ClassName=SECControlBar
WindowName=
ResourceID=0
[WorkState_v1_1.DockState.Bar-2]
BarID=59422
Bars=3
Bar#0=0
Bar#1=32769
Bar#2=0
Style=0
ExStyle=0
PrevFloating=False
MDIChild=False
AutoHide=False
AutoHidePinned=False
LastAlignedDocking=0
PctWidth=0
MRUFloatCX=0
MRUFloatCY=0
MRUHorzDockCX=0
MRUHorzDockCY=0
MRUVertDockCX=0
MRUVertDockCY=0
MRUDockingState=0
DockingStyle=0
TypeID=0
ClassName=
WindowName=
ResourceID=0
[WorkState_v1_1.DockState.Bar-3]
BarID=32769
Visible=False
XPos=0
YPos=0
Docking=True
MRUDockID=0
MRUDockLeftPos=12
MRUDockTopPos=32
MRUDockRightPos=12
MRUDockBottomPos=32
MRUFloatStyle=4
MRUFloatXPos=-4
MRUFloatYPos=0
Style=36686
ExStyle=0
PrevFloating=False
MDIChild=False
AutoHide=False
AutoHidePinned=False
LastAlignedDocking=0
PctWidth=1000000
MRUFloatCX=300
MRUFloatCY=180
MRUHorzDockCX=300
MRUHorzDockCY=180
MRUVertDockCX=150
MRUVertDockCY=180
MRUDockingState=0
DockingStyle=32768
TypeID=0
ClassName=SECControlBar
WindowName=
ResourceID=0
[WorkState_v1_1.DockState.Bar-4]
BarID=59420
Bars=3
Bar#0=0
Bar#1=32770
Bar#2=0
Style=0
ExStyle=0
PrevFloating=False
MDIChild=False
AutoHide=False
AutoHidePinned=False
LastAlignedDocking=0
PctWidth=0
MRUFloatCX=0
MRUFloatCY=0
MRUHorzDockCX=0
MRUHorzDockCY=0
MRUVertDockCX=0
MRUVertDockCY=0
MRUDockingState=0
DockingStyle=0
TypeID=0
ClassName=
WindowName=
ResourceID=0
[WorkState_v1_1.DockState.Bar-5]
BarID=32770
Visible=False
XPos=0
YPos=0
Docking=True
MRUDockID=0
MRUDockLeftPos=12
MRUDockTopPos=32
MRUDockRightPos=12
MRUDockBottomPos=32
MRUFloatStyle=4
MRUFloatXPos=-4
MRUFloatYPos=0
Style=8014
ExStyle=0
PrevFloating=False
MDIChild=False
AutoHide=False
AutoHidePinned=False
LastAlignedDocking=0
PctWidth=1000000
MRUFloatCX=300
MRUFloatCY=180
MRUHorzDockCX=300
MRUHorzDockCY=180
MRUVertDockCX=150
MRUVertDockCY=180
MRUDockingState=0
DockingStyle=4096
TypeID=0
ClassName=SECControlBar
WindowName=
ResourceID=0
[WorkState_v1_1.DockState.Bar-6]
BarID=59421
Bars=3
Bar#0=0
Bar#1=32771
Bar#2=0
Style=0
ExStyle=0
PrevFloating=False
MDIChild=False
AutoHide=False
AutoHidePinned=False
LastAlignedDocking=0
PctWidth=0
MRUFloatCX=0
MRUFloatCY=0
MRUHorzDockCX=0
MRUHorzDockCY=0
MRUVertDockCX=0
MRUVertDockCY=0
MRUDockingState=0
DockingStyle=0
TypeID=0
ClassName=
WindowName=
ResourceID=0
[WorkState_v1_1.DockState.Bar-7]
BarID=32771
Visible=False
XPos=0
YPos=0
Docking=True
MRUDockID=0
MRUDockLeftPos=12
MRUDockTopPos=32
MRUDockRightPos=12
MRUDockBottomPos=32
MRUFloatStyle=4
MRUFloatXPos=-4
MRUFloatYPos=0
Style=20302
ExStyle=0
PrevFloating=False
MDIChild=False
AutoHide=False
AutoHidePinned=False
LastAlignedDocking=0
PctWidth=1000000
MRUFloatCX=300
MRUFloatCY=180
MRUHorzDockCX=300
MRUHorzDockCY=180
MRUVertDockCX=150
MRUVertDockCY=180
MRUDockingState=0
DockingStyle=16384
TypeID=0
ClassName=SECControlBar
WindowName=
ResourceID=0
[WorkState_v1_1.DockState.Bar-8]
BarID=59392
Docking=True
MRUDockID=0
MRUDockLeftPos=-1
MRUDockTopPos=-1
MRUDockRightPos=177
MRUDockBottomPos=29
MRUFloatStyle=8196
MRUFloatXPos=-4
MRUFloatYPos=0
Style=12212
ExStyle=131852
PrevFloating=False
MDIChild=False
AutoHide=False
AutoHidePinned=False
LastAlignedDocking=0
PctWidth=30821
MRUFloatCX=0
MRUFloatCY=0
MRUHorzDockCX=178
MRUHorzDockCY=30
MRUVertDockCX=0
MRUVertDockCY=0
MRUDockingState=0
DockingStyle=61440
TypeID=14946
ClassName=SECCustomToolBar
WindowName=Edit
ResourceID=0
ptn_Child1=ToolBarInfoEx
[WorkState_v1_1.DockState.Bar-8.ToolBarInfoEx]
Title=Edit
Buttons=HAJCAAAAAAGAJCAAAAAAIAJCAAAAAAJAJCAAAAAAMAJCAAAAAADBJCAAAAAAHCJCAAAAAA
[WorkState_v1_1.DockState.Bar-9]
BarID=59397
XPos=520
Docking=True
MRUDockID=0
MRUDockLeftPos=520
MRUDockTopPos=32
MRUDockRightPos=733
MRUDockBottomPos=62
MRUFloatStyle=8196
MRUFloatXPos=-4
MRUFloatYPos=0
Style=12212
ExStyle=131852
PrevFloating=False
MDIChild=False
AutoHide=False
AutoHidePinned=False
LastAlignedDocking=0
PctWidth=30821
MRUFloatCX=0
MRUFloatCY=0
MRUHorzDockCX=213
MRUHorzDockCY=30
MRUVertDockCX=0
MRUVertDockCY=0
MRUDockingState=0
DockingStyle=40960
TypeID=14946
ClassName=SECCustomToolBar
WindowName=Copy
ResourceID=0
ptn_Child1=ToolBarInfoEx
[WorkState_v1_1.DockState.Bar-9.ToolBarInfoEx]
Title=Copy
Buttons=DCJCAAAAAANAJCAAAAAAABHCAAAACAGJAA
[WorkState_v1_1.DockState.Bar-10]
BarID=59398
XPos=733
Docking=True
MRUDockID=0
MRUDockLeftPos=733
MRUDockTopPos=-1
MRUDockRightPos=834
MRUDockBottomPos=29
MRUFloatStyle=8196
MRUFloatXPos=-4
MRUFloatYPos=0
Style=12212
ExStyle=131852
PrevFloating=False
MDIChild=False
AutoHide=False
AutoHidePinned=False
LastAlignedDocking=0
PctWidth=30821
MRUFloatCX=0
MRUFloatCY=0
MRUHorzDockCX=101
MRUHorzDockCY=30
MRUVertDockCX=0
MRUVertDockCY=0
MRUDockingState=0
DockingStyle=40960
TypeID=14946
ClassName=SECCustomToolBar
WindowName=Editors
ResourceID=0
ptn_Child1=ToolBarInfoEx
[WorkState_v1_1.DockState.Bar-10.ToolBarInfoEx]
Title=Editors
Buttons=MBJCAAAAAA
[WorkState_v1_1.DockState.Bar-11]
BarID=59399
XPos=834
Docking=True
MRUDockID=0
MRUDockLeftPos=834
MRUDockTopPos=-1
MRUDockRightPos=897
MRUDockBottomPos=29
MRUFloatStyle=8196
MRUFloatXPos=-4
MRUFloatYPos=0
Style=12212
ExStyle=131852
PrevFloating=False
MDIChild=False
AutoHide=False
AutoHidePinned=False
LastAlignedDocking=0
PctWidth=60501
MRUFloatCX=0
MRUFloatCY=0
MRUHorzDockCX=63
MRUHorzDockCY=30
MRUVertDockCX=0
MRUVertDockCY=0
MRUDockingState=0
DockingStyle=61440
TypeID=14946
ClassName=SECCustomToolBar
WindowName=ViewMode
ResourceID=0
ptn_Child1=ToolBarInfoEx
[WorkState_v1_1.DockState.Bar-11.ToolBarInfoEx]
Title=ViewMode
Buttons=CBHCAAAAAAGCJCAAAAAA
[WorkState_v1_1.DockState.Bar-12]
BarID=59401
XPos=897
Docking=True
MRUDockID=0
MRUDockLeftPos=897
MRUDockTopPos=-1
MRUDockRightPos=1068
MRUDockBottomPos=29
MRUFloatStyle=8196
MRUFloatXPos=-4
MRUFloatYPos=0
Style=12212
ExStyle=131852
PrevFloating=False
MDIChild=False
AutoHide=False
AutoHidePinned=False
LastAlignedDocking=0
PctWidth=121004
MRUFloatCX=0
MRUFloatCY=0
MRUHorzDockCX=171
MRUHorzDockCY=30
MRUVertDockCX=0
MRUVertDockCY=0
MRUDockingState=0
DockingStyle=61440
TypeID=14946
ClassName=SECCustomToolBar
WindowName=3dTools
ResourceID=0
ptn_Child1=ToolBarInfoEx
[WorkState_v1_1.DockState.Bar-12.ToolBarInfoEx]
Title=3dTools
Buttons=NBJCAAAAAAOBJCAAAAAAAAAAAAAAAAECJCAAAAAAIBJCAAAAAAJBJCAAAAAAAAAAAAAAAAKBJCAAAAAA
[WorkState_v1_1.DockState.Bar-13]
BarID=59400
XPos=1068
Docking=True
MRUDockID=0
MRUDockLeftPos=1068
MRUDockTopPos=-1
MRUDockRightPos=1181
MRUDockBottomPos=29
MRUFloatStyle=8196
MRUFloatXPos=-4
MRUFloatYPos=0
Style=12212
ExStyle=131852
PrevFloating=False
MDIChild=False
AutoHide=False
AutoHidePinned=False
LastAlignedDocking=0
PctWidth=242009
MRUFloatCX=0
MRUFloatCY=0
MRUHorzDockCX=113
MRUHorzDockCY=30
MRUVertDockCX=0
MRUVertDockCY=0
MRUDockingState=0
DockingStyle=40960
TypeID=14946
ClassName=SECCustomToolBar
WindowName=Tools
ResourceID=0
ptn_Child1=ToolBarInfoEx
[WorkState_v1_1.DockState.Bar-13.ToolBarInfoEx]
Title=Tools
Buttons=FBHCAAAACACDAAEBJCAAAAAABDJCAAAAAA
[WorkState_v1_1.DockState.Bar-14]
BarID=59402
XPos=1181
Docking=True
MRUDockID=0
MRUDockLeftPos=1181
MRUDockTopPos=-1
MRUDockRightPos=1267
MRUDockBottomPos=29
MRUFloatStyle=8196
MRUFloatXPos=-4
MRUFloatYPos=0
Style=12212
ExStyle=131852
PrevFloating=False
MDIChild=False
AutoHide=False
AutoHidePinned=False
LastAlignedDocking=0
PctWidth=484022
MRUFloatCX=0
MRUFloatCY=0
MRUHorzDockCX=86
MRUHorzDockCY=30
MRUVertDockCX=0
MRUVertDockCY=0
MRUDockingState=0
DockingStyle=40960
TypeID=14946
ClassName=SECCustomToolBar
WindowName=SABR
ResourceID=0
ptn_Child1=ToolBarInfoEx
[WorkState_v1_1.DockState.Bar-14.ToolBarInfoEx]
Title=SABR
Buttons=DDJCAAAAAAEDJCAAAAAAHDJCAAAAAA
[WorkState_v1_1.ToolBarMgr]
ToolTips=True
CoolLook=True
LargeButtons=False
