//
// WebSockets IIO Throttle
//
// RESTConnection Throttle:
//	- WindowSize		Specified by the Exchange. Set here in milliseconds
//	- NbrMsgsInWindow	Number of messages the user is allowed to sent in the
//				time window - defined by the exchange
//	- NbrRESTConnections	Number of REST connections to be created by the IIO.
//				A lower number restricts throughput
//				Deribit limit this value to 8 by returning an error when number
//				of outstanding requests is greater than 8
//
// The user's values can be looked up on the deribit web site on the api page - where the
// client and secret keys are.
//
PARAMETER ThrottleControl	"K:DeribitWSIIO"	"RESTConnection,1000,200,8"

//PARAMETER DataExchangeSettings "" "172.31.3.95:4721"
//PARAMETER ActProtocolSettings "" "172.31.3.95:4722"
  /// Use this one for API

PARAMETER ExpirationTime "M:XBIT,b:BTC,S:O" "080000"
PARAMETER ExpirationTime "M:XBIT,b:BTC,S:F" "080000"
