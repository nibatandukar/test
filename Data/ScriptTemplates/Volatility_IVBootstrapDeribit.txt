// Script:  IVBootstrap
// Type:    Volatility
// Created: 14.11.2016 12:19:04
// Author:  webdemoXS on SPLATITUDE

// Note: this script uses PE24 for ATM guess

const vol_guess = 60; // Last ditch guess at ATM vol if everything fails

// Start with IVSMOOTH
v = IVSMOOTH / 100;

// If that doesn't exist, check cutoffs
if not isvalue(v) then
	if STRIKE < IVWINGSTRIKEDOWN then
		v = IVSMOOTHDOWN / 100;
	elsif STRIKE > IVWINGSTRIKEUP then
		v = IVSMOOTHUP / 100;
	end
end

// Failing that, use either PE24 or a hardcoded guess
if PE24 and not isvalue(v) then 
	v = PE24/100;
elsif not isvalue(v) then
	v = vol_guess/100;
end

VOLATILITY = v;
