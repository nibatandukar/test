// Script:  ActantHQ
// Type:    Hidden Quote
// Created:  8.02.2013 18:53:17
// Author:  Splat

// Version information
// 1 - Initial version
// 2 - Uses valuation::uspread() for pad
//			Quantity multiplier added
// 3,4 - No change
// 5 - Fixed prounddown to use parameter to avoid error in case of 0

// To be used ONLY with ActantQuote quote script
// Uses relevant min edge and quantity defined in quote script
// Invalidates when midmarket is used as theoretical (SAFETY = 0)
// Also invalidates for wide bids and wide offers, checked individually

// Constant parameters

const qty_mult = 1;						// Quantity multiplier

// Check for PIW1
if not(isvalue(PIW1)) then
	tracet("HQ Error - ActantQuote script not assigned");
end

// Retrieve parameters set in quote script
sfty = PIW1;
min_edge_bid = PIW3;
min_edge_ask = PIW4;

// Set underlying width pad 
pad = abs(DELTA * valuation::uspread() / 2);

// Validate quantity and set price
if min_edge_bid and sfty then
	my_nbid = PIW5 * qty_mult;
else
	my_nbid = 0;
end

if min_edge_ask and sfty then
	my_nask = PIW6 * qty_mult;
else
	my_nask = 0;
end
	
my_bid = prounddown(ADJTHEOR - min_edge_bid - pad,0);
my_ask = proundup(ADJTHEOR + min_edge_ask + pad);

// Set Quotes
QNBID = my_nbid;
QBID = my_bid;
QASK = my_ask;
QNASK = my_nask;





