// Script:  ActantStrategy
// Type:    OSE HQ/COA/COLA/AIMS
// Created: 21.02.2013 11:53:59
// Author:  Splat on SPLATITUDE

// Version information
// 1 - Initial version
// 2 - Uses valuation::uspread() for pad
//   - Logging option added
//   - Added fixed strategy quantity multiplier constant (ideally for AIMS)
// 3 - Ratio, edge, qty, expcount added to PIW for use in ExStream/RFQ
// 4 - added PE18, for optional edge/qty skew factor
//   - in script text, t/f capitalized when skewed by factor
//   - edge_factor can now be 0, which will use vol difference (edge*VEGA)
//   - deals with buy-writes without penalizing stock ratio
// 5 - Option to skew theo based on skews/overrides from legs, capped at theo
//   - Populates PIW4/6 for ActantRFQ
//   - Deals with FUTURE/STOCK legs
//   - Vola strategies on CME now use SRATIO for proper scaling
//   - Added PI1 and PI2 for strategy-level edge override
//   - Prounddown changed to return error if rounding up
//   - Underlying pad now accounts for deliverables

// Uses min edge and quantity based on strategy delta, PE from furthest leg
// Multiplies edge/divides qty by the max # of units on one side (BUY or SELL)

// *** If hidden quoting and user wishes to autoenable, uncomment the
//     following line:
//if not seriesenabled("HQuote") then seriesenable("HQuote"); end

// Constant parameters

const strat_mult = 1;				// quantity multiplier for all strategies
const logging = 1;					// 0=no logging, 1=log spread info (log file)
const low_delta = 0.30;			// delta cutoff 1st tier
const mid_delta = 0.60;			// delta cutoff 2nd tier
const high_delta = 0.90;		// delta cutoff 3rd tier (higher is 4th tier) 
const edge_factor = 1;			// All edges divided by this number, so if user
												// wants to specify edges in # of ticks, set
												// this to the number of ticks in one unit move
												// For example, if 64ths, set this to 64
												// If pennies, set to 100 (so edge of 2 -> 0.02)
                             // * Setting to zero will use "vol edge" instead,
                             // meaning all values will be multiplied by VEGA
const min_vega = 0.001;      // Minimum VEGA to use when using "vol edge"
                             // * Do not use zero here
const use_skew = 0;					// 0=no skew, 1=offset strategy theo when leg min
												// edges are asymmetrical, capped at actual theo

// Variable parameters

// Underlying
// UNDER.PIT	All script flags disable strategies
// UNDER.PI1	Disabled for strategies, use PE17 instead
// UNDER.PI2	Disabled for strategies, use PE17 instead

// Expiration
// PET		All script flags disable strategies
// 1st tier low (under low_delta)
// PE1		Quantity
// PE2		Minimum edge to join
// PE3		Maximum edge to spread (used only if PE2 blank)
//
// 2nd tier mid (low_delta to mid_delta)
// PE4		Quantity
// PE5		Minimum edge to join
// PE6		Maximum edge to spread (used only if PE5 blank)
//
// 3rd tier high (mid_delta to high_delta)
// PE7		Quantity
// PE8		Minimum edge to join
// PE9		Maximum edge to spread (used only if PE8 blank)
//
// 4th tier deep (high_delta and up)
// PE10		Quantity
// PE11		Minimum edge to join
// PE12		Maximum edge to spread (used only if PE11 blank)
//
// PE13		Join quantity multiplier (not used in strategy script)
// PE14		Bid quantity multiplier (not used in strategy script - use PE16)
// PE15		Ask quantity multiplier (not used in strategy script - use PE16)
// PE16		Strategy quantity multiplier (default 1)
// PE17		Strategy edge multiplier (default 1)
// PE18		Five digit number corresponding to bias - digits represent delta
//				bins for calls 0-20 up to 80-100, and are numbers from 1-9, where
//				1 is strong buy, 5 is neutral, 9 is strong sell
//				Note that call bins apply to corresponding puts, so 15 delta call
//				is in the same bin as 85 delta put
//				Bias will apply as a factor for qtys, and as a multiplier for edges
//				If edge is overridden with PI1/PI2, then edge bias will be ignored

// Option series
// PIT		All script flags disable strategies
// PI1		Disabled for strategies, use PE17 instead
// PI2		Disabled for strategies, use PE17 instead

// Strategies themselves
// PI1    Bid edge override, this strategy only
// PI2    Ask edge override, this strategy only
	
// *** QUOTE SCRIPT USES PIW1-PIW9 ***

// Identify number of legs and number of units on one side, validate
if UNDER.PIT then
	valid = 0;
else
	valid = 1;
end

ratio_buy = 0;
ratio_sell = 0;
furthest = -1;
qty_mult = 1;
edge_mult = 1;
offset = 0;

// Loop through strategy legs
for i = 0 to numlegs() - 1 do
	if leginfo("ITYPE", i) == CALL or leginfo("ITYPE", i) == PUT then
		// Set edge factor for each leg as appropriate
		if edge_factor then
			factor = 1/edge_factor;
		else
			factor = max(leginfo("VEGA", i), min_vega);
		end

		// Calculate asymmetrical edge leg offset if used
		if use_skew then
			if leginfo("PIW3", i) and leginfo("PIW4", i) then
				offset +=leginfo("SRATIO",i)*(leginfo("PIW4",i)-leginfo("PIW3",i))/2;
			end
		end

		if furthest < leginfo("ECOUNT", i) then
			// Set parameters on currently furthest month
			furthest = leginfo("ECOUNT", i);
			if leginfo("PE16", i) then qty_mult = leginfo("PE16", i); end
			if leginfo("PE17", i) then edge_mult = leginfo("PE17", i); end
			// Set edge and quantity based on strategy delta range
			if abs(DELTA) < low_delta then
				// Low delta
				text_bid = "L";
				text_ask = "L";
				qty = floor(leginfo("PE1", i) * qty_mult);
				if leginfo("PE2", i) then 
					edge = leginfo("PE2", i) * edge_mult * factor;
				elsif leginfo("PE3", i) then 
					edge = leginfo("PE3", i) * edge_mult * factor;
				else
					valid = 0;
					edge = 0;
				end
			elsif abs(DELTA) < mid_delta then
				// Mid delta
				text_bid = "M";
				text_ask = "M";
				qty = floor(leginfo("PE4", i) * qty_mult);
				if leginfo("PE5", i) then 
					edge = leginfo("PE5", i) * edge_mult * factor;
				elsif leginfo("PE6", i) then 
					edge = leginfo("PE6", i) * edge_mult * factor;
				else
					valid = 0;
					edge = 0;
				end
			elsif abs(DELTA) < high_delta then
				// High delta
				text_bid = "H";
				text_ask = "H";
				qty = floor(leginfo("PE7", i) * qty_mult);
				if leginfo("PE8", i) then 
					edge = leginfo("PE8", i) * edge_mult * factor;
				elsif leginfo("PE9", i) then 
					edge = leginfo("PE9", i) * edge_mult * factor;
				else
					valid = 0;
					edge = 0;
				end
			else
				// Deep delta
				text_bid = "D";
				text_ask = "D";
				qty = floor(leginfo("PE10", i) * qty_mult);
				if leginfo("PE11", i) then 
					edge = leginfo("PE11", i) * edge_mult * factor;
				elsif leginfo("PE12", i) then 
					edge = leginfo("PE12", i) * edge_mult * factor;
				else
					valid = 0;
					edge = 0;
				end
			end
		end
		// Set ratio factor to larger of buy units and sell units
		ratio = leginfo("SRATIO", i) * leginfo("CSIZE", i) / CSIZE;
		if ratio > 0 then
			ratio_buy += abs(ratio);
		else
			ratio_sell += abs(ratio);
		end
		// Invalidate if any script flags
		if leginfo("PET", i) or leginfo("PIT", i) then
			valid = 0;
		end
	end
end

// Set underlying width pad 
// In case of deliverable underlying, calculate pad per deliverable
if strat::numdeliverables() then
	pad = 0;
	for i = 0 to strat::numdeliverables() - 1 do
		deliv = nav::deliverable(i);
		pad = pad + abs(deliv.valuation::uspread() * del::delta(strat::deliverableordinal(i)) * 0.5);
	end
else
	pad = abs(DELTA * valuation::uspread() / 2);
end

// Adjust edge and quantity per calculated ratio and set price
ratio = max(ratio_buy, ratio_sell);
qty = floor(qty * strat_mult / ratio);
edge = edge * ratio;
if PI1 then
	// Override bid edge
	same = prounddown(ADJTHEOR - PI1*factor - pad,1);
else	
	same = prounddown(min(ADJTHEOR, ADJTHEOR + offset - edge - pad),1);
end
if PI2 then 
	// Override ask edge
	oppos = proundup(ADJTHEOR + PI2*factor + pad);
else	
	oppos = proundup(max(ADJTHEOR, ADJTHEOR + offset + edge + pad));
end	
text_bid = strcat(text_bid, ptos(ratio));
text_ask = strcat(text_ask, ptos(ratio));

// Initialize quotes and validate
QNBID = 0;
QBID = 0;
QASK = 0;
QNASK = 0;	
TBID = "SKIP";
TASK = "SKIP";

if valid then
// Check if customer side has been established
	if MESIDE == MEBUY then
	// Customer buying
		QASK = oppos;
		QNASK = qty;
		TASK = text_ask;
	elsif MESIDE == MESELL then
	// Customer selling
		QNBID = qty;
		QBID = same;
		TBID = text_bid;
	else
	// Both sides possible
		QNBID = qty;
		QBID = same;
		QASK = oppos;
		QNASK = qty;
		TBID = text_bid;
		TASK = text_ask;
	end
	SAFETY = 1;
end

if logging then
	logger::infod("%N - Qty:%g Same:%g Opp:%g TBID:%s TASK:%s", qty, same, oppos, TBID, TASK);
end

// Assign PIW for each strategy
if valid then PIW1 = ratio; else PIW1 = 0; end
PIW3 = edge;
PIW4 = edge;
PIW5 = qty;
PIW6 = qty;
PIW7 = furthest;
