// Script:  Spline7
// Type:    Volatility

// Seven point spline, based on standard deviations derived from ATM vol
// Enter ATM vol into PE24, and then put multipliers of that vol into each
// SD level (so all 1's would be a flat vol)
// Upside and Downside Cutoffs allow for a linear slope 
// to be applied after cutoffs. They are entered in moneyness.
// Spacing allow user to alter spacing for sparsely populated curves or 
// when near expiration, when splines are less effective
// RefUprice is the UPrice on which to base adjustments to Vol and Skew Path
// VolPath expressed in vol points per point move in underlying
// Skew Adjustment expressed in vol points for a 1SD option
// Kurt Adjustment expressed in vol points for a 1SD option

// Version information
// 1 - Initial version
// 2 - Restrict skew and kurtosis adjustments for values oustide cutoff range
//   - Vol must be at least 1 percent
// 3 - Allows for skew/kurt adjustments for upside and downside optionally
//   - Minor revisions (mostly formatting)
//   - Script will fall back to last good vol if the calculated vol is zero
// 4,5 No change

// PE21  -2 SD 
// PE22  -1 SD
// PE23 -.5 SD
// PE24 ATM vol
// PE25 +.5 SD
// PE26  +1 SD
// PE27  +2 SD
// PE28 Downside Cutoff
// PE29 Upside Cutoff
// PE30 SD Spread Basis (Defaults to 1)
// PE31 Ref Uprice
// PE32 Vol Path slope in Vol points per U point
// PE33 Vol Path curvature (Vol Points at one SD move)
// PE34 Downside Vol Slope
// PE35 Upside Vol Slope
// PE36 Amount to move -1SD Up for skew 
// PE37 Amount to move -1SD Up for kurt
// PE38 Skew Slope
// PE39 Amount to move -1SD Up for skew (optional, upside only)
// PE40 Amount to move -1SD Up for kurt (optional, upside only)

//Set Spacing
if PE30 > 0 then 
  spacing = PE30;
else 
  spacing = 1;
end

//Set forward, change from reference, and ATM vol
forward = math::forward(UPRICE,PVDIV,SRATE,TEXP);
fwdChange = forward- math::forward(PE31,PVDIV,SRATE,TEXP);
atmvol = (PE24+fwdChange*PE32+fwdChange*fwdChange*PE33)/100;

if UPRICE then 
  // Set skew, kurtosis (different for upside if PE39 or PE40)
  moneyness = math::moneyness(STRIKE,forward,atmvol,TEXP);
  if moneyness >= 0 and PE39 then
    skewadj = PE39 + fwdChange*PE38;
  else
    skewadj = PE36 + fwdChange*PE38;
  end
  if moneyness >= 0 and PE40 then
    kurtadj = PE40;
  else
    kurtadj=PE37;
  end
  
  // Implement cutoffs with sloped surface
  downcutoff = PE28;
  upcutoff = PE29;
  if(moneyness<downcutoff)then
		adj= (moneyness-downcutoff)*PE34;
  elsif(moneyness>upcutoff)then
  		adj= (moneyness-upcutoff)*PE35;
  else
		adj=0;
  end
  
  // Calculate vol spline before skew and kurtosis adjustments
  v=math::spline(max(min(upcutoff,moneyness),downcutoff), -2*spacing,PE21*atmvol, -spacing,PE22*atmvol , -0.5*spacing,PE23*atmvol, 0,atmvol, 0.5*spacing,PE25*atmvol, spacing,PE26*atmvol, 2*spacing,PE27*atmvol);
  
  // Set vol(adjusted for skew/kurtosis within cutoff range)
  moneyness=max(downcutoff,min(moneyness,upcutoff));
  v=max(0.01, v+adj+moneyness*SkewAdj+kurtadj*moneyness*moneyness);
else
  // If no UPRICE available, fall back to atm vol
  v = atmvol;
end

// Use previous vol if something goes wrong
if v then
	VOLATILITY = v;
else
	VOLATILITY = VOLA;
end
